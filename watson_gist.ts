import fs from 'fs';
import os from 'os';
import axios from 'axios';

const file = `${os.homedir()}/.config/watson/frames`

interface Gist {
  description: string
  url: string
  owner: string
  id: string
}

const url = "https://api.github.com/gists"

const headers = {
  'Accept': 'application/vnd.github+json',
  'Authorization': `token ${process.env.GITHUB_PRIVATE_TOKEN}`
}

const data = {
  description: `Watson frames ${new Date}`,
  'public': false,
  files: {
    'frames': {
      content: fs.readFileSync(file, 'utf-8')
    }
  }
}

async function createNew() {
  axios.post(url, data, {headers: headers})
  .then((res) => {
    if (res.status === 201) {
      console.log("Created new gist")
    }
  })
}

async function upload(frames: Gist[]) {
  if (!frames.length) {
    console.log("No Watson frames found")
    return
  }

  axios.patch(frames[0].url, data, {headers: headers})
  .then((res) => {
    if (res.status === 200) {
      console.log("Uploaded gist")
    }
  })
}

async function download(frames: Gist[]) {
  if (!frames.length) {
    console.log("No Watson frames found")
    return
  }

  const gistUrl = `https://gist.githubusercontent.com/${frames[0].owner}/${frames[0].id}/raw`
  const writer = fs.createWriteStream(file)

  axios.get(gistUrl, {responseType: 'stream'})
  .then((res) => {
    if (res.status === 200) {
      console.log("Downloaded gist")
      res.data.pipe(writer)
    }
  })
}

async function get(): Promise<Gist[]> {
  let gists: Gist[] = []

  await axios.get(url, {headers: headers}).then((res) => {
    for (let gist of res.data) {
      gists.push({
        description: gist.description,
        url: gist.url,
        owner: gist.owner.login,
        id: gist.id
      })
    }
  })

  return gists
}

async function main() {
  let gists: Gist[] = await get()

  let frames = gists.filter((gist) => {
    return gist.description.includes("Watson frames")
  })

  if (!process.argv[2]) {
    if (gists.length === 0 || !frames.length) {
      console.log("No Watson frames found... uploading new gist")
      createNew()
      return
    }

    console.log("Watson frames found... run script with download/upload argument")
  }

  if (process.argv[2] === 'download') {
    download(frames)
  }

  if (process.argv[2] === 'upload') {
    upload(frames)
  }
}

main()
