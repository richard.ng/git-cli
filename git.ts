import { Git, Repo } from './interface';
import axios from 'axios';

class Gitlab implements Repo {
  name: string
  message: string
  value: string

  constructor(item: any) {
    this.name = item.path
    this.message = `${item.name} (${item.ssh_url_to_repo})`
    this.value = item.ssh_url_to_repo
  }
}

class Github implements Repo {
  name: string
  message: string
  value: string

  constructor(item: any) {
    this.name = item.name
    this.message = `${item.name} (${item.ssh_url})`
    this.value = item.ssh_url
  }
}

export class Repos {
  url: string
  token: string
  headers: {}
  git: string

  constructor(repoService: Git) {
    this.url = repoService.url
    this.token = repoService.token
    this.headers = repoService.headers
    this.git = repoService.git
  }

  getRepos() {
    return axios.get(this.url, this.headers)
    .then((response) => {
      let repos = []
      for (let item of response.data) {
        let git
        if (this.git === 'gitlab') {
          git = new Gitlab(item)
        } else {
          git = new Github(item)
        }
        let repo = {
          name: git.name,
          message: git.message,
          value: git.value
        }
        repos.push(repo)
      }
      return repos
    })
  }
}
