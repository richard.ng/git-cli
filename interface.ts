export interface Repo {
  name: string
  message: string
  value: string
}


export interface Git {
  url: string
  token: string
  headers: any
  git: string
}

export interface GitIndex {
  [name: string]: Git
}
