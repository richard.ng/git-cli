import { Repos } from './git';
import { Git, GitIndex } from './interface';
const { prompt, MultiSelect, AutoComplete } = require('enquirer');
import fs from 'fs';
import os from 'os';
const spawn = require('child_process').spawn;

const devFolder = `${os.homedir()}/dev/repos`

const git: GitIndex = {
  'Gitlab': {
    url: 'https://gitlab.com/api/v4/projects?membership=true&per_page=999',
    token: `${process.env.GITLAB_PRIVATE_TOKEN}`,
    headers: {
      headers: {
        'Authorization': `Bearer ${process.env.GITLAB_PRIVATE_TOKEN}`
      }
    },
    git: 'gitlab'
  },
  'Github': {
    url: 'https://api.github.com/user/repos',
    token: `${process.env.GITHUB_PRIVATE_TOKEN}`,
    headers: {
      headers: {
        'Accept': 'application/vnd.github.v3+json',
        'Authorization': `token ${process.env.GITHUB_PRIVATE_TOKEN}`
      }
    },
    git: 'github'
  }
}

async function askRepos(repoService: Git) {
  let git = await new Repos(repoService)
  let repos = await git.getRepos()

  const prompt = new AutoComplete({
    name: 'repos',
    message: 'Which repo to clone/pull?',
    choices: repos,
    multiple: true,
    result(names: any) {
      return this.map(names);
    }
  })

  prompt.run()
  .then((answer: any) => {
    for (let foldername in answer) {
      let path = `${devFolder}/${foldername}`
      let url = `${answer[foldername]}`
      if (fs.existsSync(path)) {
        console.log('Folder already exists...Pulling...')
        spawn('git', ['-C', path, 'pull', '--no-rebase'], {stdio: 'inherit'})
      } else {
        console.log('Folder does not exist...Cloning...')
        spawn('git', ['clone', url, path], {stdio: 'inherit'})
      }
    }
  })
}

async function main() {
  await prompt({
    type: 'select',
    name: 'vcp',
    message: 'Which version control platform?',
    choices: [
      'Gitlab', 'Github'
    ]
  })
  .then((answer: any) => {
    askRepos(git[answer.vcp])
  })
}

main()
